(ns test
  (:require [aoc.primes :as primes])
  (:use clojure.test))

(deftest not-prime-2
  (is (true? (primes/prime? 2))))

(deftest not-prime-4
  (is (false? (primes/prime? 4))))

(deftest next-prime
  (is (= 103 (primes/next-prime 101))))

(deftest all-prime
  (is (true? (every? primes/prime? (primes/sieve 100)))))

(deftest sieve
  (is (= 25 (count (primes/sieve 100))))
  (is (= '(2 3 5 7) (primes/sieve 10))))

(deftest prime-filter
  (is (= 25 (count (primes/prime-filter 100))))
  (is (= '(2 3 5 7) (primes/prime-filter 10))))

(deftest sieve-equals-filter
  (is (= (set (primes/sieve 20))
         (set (primes/prime-filter 20)))))

(run-tests 'test)

