(ns hill-climbing
  (:require [aoc.route-finder :as rf]
            [clojure.string :as str])
  (:use clojure.test))

(def sample-data
  "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi")

(def aoc-data (slurp (clojure.java.io/resource "hill-climbing.txt")))

(defn- get-neighbours-fn [board]
  (let [width (count (get board 0))
        height (count board)]
    (fn [[x y]]
      (->> [[0 1] [0 -1] [1 0] [-1 0]]
           (map (fn [[dx dy]] [(+ x dx) (+ y dy)]))
           (filter (fn [[x' y']] (and (>= x' 0) (>= y' 0) (< x' width) (< y' height))))
           (filter (fn [[x' y']] (<= (get-in board [y' x']) (inc (get-in board [y x])))))
           (map (fn [[x y]] [[x y] 1]))))))

(defn- find-board-item [all-points board item]
  (->> all-points
       (filter (fn [[x y]] (= item (get-in board [y x]))))
       first))

(defn- map-board [f board]
  (mapv #(mapv f %) board))

(defn- char->value [item]
  (cond
    (= item \E) 25
    (= item \S) 0
    :else (- (int item) (int \a))))

(defn- find-shortest-route
  "The Hill Climbing Algorithm from Advent Of Code 2022 day 12"
  [data]
  (let [init-board (mapv vec (str/split-lines data))
        width (count (get init-board 0))
        height (count init-board)
        all-points (for [y (range 0 height) x (range 0 width)] [x y])
        start (find-board-item all-points init-board \S)
        finish (find-board-item all-points init-board \E)
        board (map-board char->value init-board)
        get-neighbours (get-neighbours-fn board)]
    (:score (rf/a-star start finish get-neighbours))))

(deftest hill-climbing
  (testing "Find the shortest route (hill climbing)"
    (is (= 31 (find-shortest-route sample-data)))
    (is (= 352 (find-shortest-route aoc-data)))))

(run-tests 'hill-climbing)
