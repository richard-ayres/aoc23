(ns day10)

(def infile "day10.txt")

(def lines (vec (->> infile
                     aoc.io/file->lines
                     (map vec))))
(println lines)

(def neighbour-map
  {\| [[0 -1] [0 1]]
   \- [[-1  0] [1 0]]
   \L [[0 -1] [1 0]]
   \J [[0 -1] [-1 0]]
   \7 [[-1  0] [0 1]]
   \F [[1  0] [1 0]]})

(defn point->neighbours [[x y] ch]
  (mapv (fn [[nx ny]] [(+ x nx) (+ y ny)]) (neighbour-map ch)))

(def the-map
  (into {}
        (remove nil?
                (let [w (count (first lines))
                      h (count lines)]
                  (for [x (range 0 w)
                        y (range 0 h)]
                    (let [ch (nth (nth lines y) x)]
                      (if (= ch \.)
                        nil
                        [[x y] ch])))))))

(def S (first (first (filter (fn [[_ ch]] (= ch \S)) the-map))))

(def neighbours [[-1 0] [0 -1] [1 0] [0 1]])

(defn add-point [a b]
  [(+ (a 0) (b 0)) (+ (a 1) (b 1))])

(defn get-neighbours [pt]
  (for [n [[-1 0] [0 -1] [1 0] [0 1]]]
    (add-point pt n)))

(def S-neighbours (filter #(contains? the-map %) (get-neighbours S)))

(def the-map
  (into {}
        (for [[pt ch] the-map]
          [pt (point->neighbours pt ch)])))

; [[x y] (point->neighbours [x y] ch)]
(def the-map (assoc the-map S S-neighbours))

(println
 (loop [cur (first (the-map S))
        step 2
        seen #{S cur}
        result {cur 1}]
   (println ":" cur ":" result)
   (if (or (= 9 step) (= cur S))
     result
     (let [nxt (first (filter #(not (contains? seen %)) (the-map cur)))]
       (println cur (the-map cur) nxt seen)
       (recur nxt (inc step) (conj nxt seen) (assoc result nxt step))))))

; (println the-map)
; (println "S:" S)
; (println "S neighbours:" S-neighbours)
