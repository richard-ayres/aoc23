(ns day17
  (:require [clojure.string :as str]
            [aoc.route-finder :as rf]))

;; (def infile "day17.txt")

;; (def the-map
;;   (->> infile
;;        slurp
;;        str/split-lines
;;        (map #(str/split % #""))
;;        (mapv #(mapv parse-long %))))

; (defn print-map [vvs]
;   (println (str/join "\n" (map #(str/join "" %) vvs))))
; (print-map the-map)

;; (def width (count (get the-map 0)))
;; (def height (count the-map))

(defn point-diff [[x y] [x' y']]
  [(- x x') (- y y')])

(defn to-node-list [node]
  (->> node
       (iterate :previous)
       (take-while (comp not nil?))
       (map :loc)))

(defn differences [N node]
  (->> node
       to-node-list
       (partition 2 1)    ;; (zip nodes (drop 1 nodes))
       (map #(apply point-diff %))
       (take N)))

(def wobble-allowed
  (memoize
   (fn [last-3 dir]
     (or (< (count last-3) 3)
         (not (every? (partial = dir) last-3))))))

(defn get-neighbours [the-map width height node]
  (let [[x y] (:loc node)
        last-3 (differences 3 node)]
    (->> [[0 1] [0 -1] [1 0] [-1 0]]
         (filter #(wobble-allowed last-3 %))
         (map (fn [[dx dy]] [(+ x dx) (+ y dy)]))
         (filter (fn [[x y]] (and (>= x 0) (>= y 0) (< x width) (< y height))))
         (map (fn [[x y]] [[x y] (get-in the-map [y x])])))))

(defn manhattan-distance [[x y] [x' y']]
  (+ (abs (- x x')) (abs (- y y'))))

(defn part1
  [data]
  (let [the-map (->> data
                     str/split-lines
                     (map #(str/split % #""))
                     (mapv #(mapv parse-long %)))
        width (count (get the-map 0))
        height (count the-map)
        start [0 0]
        finish [(dec width) (dec height)]
        result (rf/finder start finish
                          (partial get-neighbours the-map width height)
                          false
                          (partial manhattan-distance finish))]
    (:score result)))
