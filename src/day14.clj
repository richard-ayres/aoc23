(ns day14
  (:require [aoc.board :as b]
            [clojure.string :as str]))

(defn make-board [data]
  (b/from-strings (str/split-lines data)))

(defn move-rock-north [board x y]
  (if (< y 1)
    board
    (let [this-item (b/bget board x y)
          next-item (b/bget board x (dec y))]
      (cond
        (not= \O this-item) board
        (not= \. next-item) board
        :else (recur (b/bset (b/bset board x y \.) x (dec y) \O)
                     x
                     (dec y))))))

(defn tilt-north [board]
  (let [height (b/height board)
        width (b/width board)]
    (loop [y 1 x 0 board board]
      (cond
        (= y height) board
        (= x width)  (recur (inc y) 0 board)
        :else        (recur y (inc x) (move-rock-north board x y))))))

(defn calculate-load [board]
  (reduce +
          (let [h (b/height board)
                w (b/width board)]
            (for [y (range 0 h) x (range 0 w)]
              (if (= \O (b/bget board x y))
                (- h y)
                0)))))

(defn part1 [data]
  (->> data
       make-board
       tilt-north
       calculate-load))

(defn tilt-cycle [board]
  "Perform a whole cycle"
  (nth (iterate (comp b/rotate-right tilt-north) board) 4))

(defn find-loop
  "Perform as many tilt cycles as it takes to find a
  loop, then return a vector of
    [<iteration of loop start> 
     <cycle length>
     <repeated board>]"
  ([board] (find-loop board 0 (hash-map (b/board-hash board) 0)))
  ([board n hashes]
   (let [next-board (tilt-cycle board)
         next-hash (b/board-hash next-board)]
     (if (contains? hashes next-hash)
       (let [cycle-start (get hashes next-hash)]
         [cycle-start (- n cycle-start) next-board])
       (recur next-board
              (inc n)
              (assoc hashes next-hash n))))))

(defn part2 [data]
  (let [N 1000000000
        [from length repeated-board] (find-loop (make-board data))
        more (mod (- N from 1) length)]
    (calculate-load (nth (iterate tilt-cycle repeated-board) more))))
