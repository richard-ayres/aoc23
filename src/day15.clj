(ns day15
  (:require [aoc.coll :as coll]
            [clojure.string :as str]))

(defn hash-1a
  "Calculate the hash as per Appendix 1A"
  [s]
  (coll/foldLeft #(mod (* 17 (+ %1 (int %2))) 256) 0 s))

(defn part1 [data]
  (->> data
       str/trim-newline
       (#(str/split % #","))
       (map hash-1a)
       (reduce +)))

(defn remove-lens
  "Return a box with the lens with the given label removed"
  [label box]
  (remove #(= label (:label %)) box))

(defn update-lens
  "Update a box to have the given new lens"
  [lens box]
  (let [label (:label lens)]
    (loop [box' []                ;; collects the new version of the box
           [lens' & remaining] box]
      (cond
        ;; got to end of box without finding lens with label: return box with lens at end
        (nil? lens')             (conj box' lens)
        ;; found existing lens: replace it
        (= (:label lens') label) (concat box' (cons lens remaining))
        ;; not found lens yet
        :else                    (recur (conj box' lens') remaining)))))

(defn hashmap-op
  "Perform the given operation on the vector of boxes"
  [boxes init-op]
  (let [[_ label op focal-length] (re-matches #"(\w+)(\W)(\d*)" init-op)]
    (update boxes (hash-1a label)
            (case op
              "=" #(update-lens {:label label :focal-length (parse-long focal-length)} %)
              "-" #(remove-lens label %)))))

(defn focus-power
  "Calculate 'focus power' of a box"
  [box]
  (reduce +
          (for [[slot lens] (coll/enumerate 1 box)]
            (* slot (:focal-length lens)))))

(defn part2 [data]
  (let [boxes (vec (repeat 256 []))]
    (->> data
         str/trim-newline
         (#(str/split % #","))
         (coll/foldLeft hashmap-op boxes)   ;; perform HASHMAP algorithm
         (map focus-power)                  ;; get focusing power of each box
         (coll/enumerate 1)                 ;; new sequence with box number
         (map #(apply * %))                 ;; multiple box number by focusing power of box
         (reduce +))))

