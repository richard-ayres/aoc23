(ns day18
  (:require [clojure.string :as str]
            [aoc.string :as string]
            [clojure.spec.alpha :as s]))

(defn line->dig-plan
  [text]
  (let [[_ dir cnt colour] (re-matches #"([RLUD]) (\d+) \((.+?)\)" text)]
    [dir (parse-long cnt) colour]))

(defn move-pos
  [[x y] dir cnt]
  (case dir
    "R" [(+ x cnt) y]
    "L" [(- x cnt) y]
    "D" [x (+ y cnt)]
    "U" [x (- y cnt)]))

(defn parse-entry-1
  [[dir cnt _]] [dir cnt])

(defn parse-entry-2
  [entry]
  (let [[_ _ colour] entry]
    (let [length (bigint (read-string (str "0x" (string/substr colour 1 -1))))
          dir ({"0" "R" "1" "D" "2" "L" "3" "U"} (string/substr colour -1))]
      [dir length])))

;; (s/assert (partial = ["L" 5N]) (parse-entry-2 ["U" 2 "#52"]))
;; (s/assert (partial = ["U" 2085N]) (parse-entry-2 [0 0 "#8253"]))

(defn dig-plan->vertices
  ([dig-plan] (dig-plan->vertices parse-entry-1 dig-plan))
  ([parse-entry dig-plan]
   (loop [dig-plan dig-plan
          vertices '([0 0])]
     (if-some [[entry & dig-plan'] dig-plan]
       (let [[dir cnt] (parse-entry entry)
             pos' (move-pos (first vertices) dir cnt)]
         (recur dig-plan' (cons pos' vertices)))
       (reverse vertices)))))

(defn dig-plan-area
  ([dig-plan] (dig-plan-area parse-entry-1 dig-plan))
  ([parse-entry dig-plan]
   (->> (for [[[ax ay] [bx by]] (partition 2 1 (dig-plan->vertices parse-entry dig-plan))]
          (+ (- (* ax by) (* ay bx))
             (abs (+ (- bx ax) (- by ay)))))
        (reduce +)        ;; sum
        (#(/ % 2))        ;; halve
        inc)))

(defn preprocess
  [data] (str/split-lines data))

(defn part1
  [data]
  (->> data
       (map line->dig-plan)
       dig-plan-area))

(defn part2
  [data]
  (->> data
       (map line->dig-plan)
       (dig-plan-area parse-entry-2)))
