(ns day19
  (:require [clojure.string :as str]))

(defn- text->rule
  [text]
  ;; (cond
  ;;   (= "A" text) true
  ;;   (= "R" text) false
  ;;   :else
  (let [m (re-matches #"(\w)([<>])(\d+):(\w+)" text)]
    (if (nil? m)
      text
      (let [[_ rating op value target] m]
        [rating op (parse-long value) target]))))

(defn- text->workflow
  [text]
  (let [[_ id rules] (re-matches #"(\w+)\{(.+)\}" text)]
    [id (map text->rule (str/split rules #","))]))

(defn- text->part [text]
  (->> text
       (re-seq #"(\w)=(\d+)")
       (map (fn [[_ k v]] [k (parse-long v)]))
       (into {})))

(defn preprocess
  "Process incoming AoC data into a 2-vec of [workflows parts]"
  [data]
  (let [[workflows parts]
        (->> data
             str/split-lines
             (split-with (complement str/blank?)))]
    [(into {"A" true "R" false} (map text->workflow workflows))
     (map text->part (drop 1 parts))]))

(defn- matches-workflow?
  "Test if part matches the provided condition.
  If condition is just text, return true"
  [part condition]
  (if (string? condition)
    true
    (let [[f op v] condition]
      (case op
        "<" (< (part f) v)
        ">" (> (part f) v)))))

(defn- process-workflow
  "Returns the name of the next workflow"
  [workflow part]
  (let [found (first (filter (partial matches-workflow? part) workflow))]
    (if (string? found)
      found          ;; direct
      (last found))));; name of next workflow

(defn- accept-part?
  ([workflows part] (accept-part? workflows part "in"))
  ([workflows part wf-name]
   (let [workflow (workflows wf-name)]
     (if (boolean? workflow)
       workflow
       (recur workflows part (process-workflow workflow part))))))

(defn part1
  [[workflows parts]]
  (->> parts
       (filter (partial accept-part? workflows))
       (map #(apply + (vals %)))
       (reduce +)))

(def part2 (constantly 0))
