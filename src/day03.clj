(ns day03
  (:require [clojure.string :as str]))

(defn- is-digit? [ch]
  (let [ch (int ch)]
    (and (>= ch 0x30) (<= ch 0x39))))

(defn- contains-symbol? [text offset cnt]
  (let [start (min (max 0 offset) (dec (count text)))
        end (min (count text) (+ offset cnt))]
    (and (not (nil? text))
         (not (every? #(or (is-digit? %) (= \. %)) (subs text start end))))))

(defn- find-part-number-sum [prev-line this-line next-line]
  (loop [offset 0
         numbers (re-seq #"\d+" this-line)
         parts '()]
    (if-some [[number & remaining] numbers]
      (let [number-len (count number)
            index (str/index-of this-line number offset)]
        (if (or (contains-symbol? prev-line (dec index) (+ 2 number-len))
                (contains-symbol? this-line (dec index) 1)
                (contains-symbol? this-line (+ index number-len) 1)
                (contains-symbol? next-line (dec index) (+ 2 number-len)))
          (recur (+ index number-len) remaining (cons (parse-long number) parts))
          (recur (+ index number-len) remaining parts)))
      (apply + parts))))

(defn- runit [lines sum-fn]
  (loop [prev-line nil
         lines lines
         the-sum 0]
    (if-some [[this-line & remaining] lines]
      (let [next-line (first remaining)]
        (recur this-line
               remaining
               (+ the-sum (sum-fn prev-line this-line next-line))))
      the-sum)))

(defn part1 [data]
  (->> data
       str/split-lines
       (#(runit % find-part-number-sum))))

;; (defn- offset-where
;;   ([f xs] (offset-where f xs 0))
;;   ([f xs offset] (first (filter #(f (get xs %)) (range offset (count xs))))))

(defn- find-ratios [prev-line this-line next-line offset]
  (defn- extract-number [text offset]
    (if (nil? text)
      nil
      (let [start (max 0 (- offset 3))
            end (min (count text) (+ offset 4))]
        (println text start end)
        (re-matches #"(\d+)?[\.\*](\d+)?" (subs text start end)))))

  (let [[_ tl tr] (extract-number prev-line offset)
        [_ l r]   (extract-number this-line offset)
        [_ bl br] (extract-number next-line offset)
        ratio    (filter (comp not nil?) (list tl tr l r bl br))]
    (println "ratio" ratio tl tr l r bl br)
    (if (= 2 (count ratio))
      ratio
      nil)))

