(ns day08
  (:require [clojure.string :as str]
            [aoc.math :as math]))

(defn- text->node
  "Take a string from the input and return a [node [left right]]"
  [text]
  (let [[_ node left right] (re-matches #"(.+?) = \((.+?), (.+?)\)" text)]
    [node [left right]]))

(defn- the-map
  "Map of node -> [left right] pair"
  [data]
  (into {} (map text->node (drop 2 (str/split-lines data)))))

(defn- the-route
  "Vector of 0 and 1 indexes into nodes in the map"   ;; use vector as MUCH quicker
  [data]
  (vec (map {\L 0 \R 1} (first (str/split-lines data)))))

(defn- count_steps
  "Count the number of steps until finished? returns true"
  [the-map the-route finished? start]
  (loop [steps 0 loc start]
    (if (finished? loc)
      steps
      (recur (inc steps)
             ((the-map loc) (the-route (mod steps (count the-route))))))))

(defn part1 [data]
  (count_steps (the-map data) (the-route data) #(= % "ZZZ") "AAA"))

(defn part2 [data]
  (let [the-map (the-map data)
        the-route (the-route data)]
    (->> (keys the-map)
         (filter #(str/ends-with? % "A"))
         (map (partial count_steps the-map the-route #(str/ends-with? % "Z")))
         (reduce math/lcm))))
