(ns day09
  (:require [clojure.string :as str]))

(defn lines [data]
  (->> data
       str/split-lines
       (map #(str/split % #" "))
       (map #(map parse-long %))))

(defn differences
  "Return the differences of numbers in the sequence"
  [xs] (map #(apply - %) (map vector (rest xs) xs)))

(defn extrapolate
  "Repeatedly find the differences of numbers in a sequence
  until all differences are zero. Sum the last value in the
  all differences sequences and make this the extrapolated value"
  [xs] (loop [xs xs acc 0]
         (if (every? zero? xs)
           acc
           (recur (differences xs)
                  (+ acc (last xs))))))

(defn part1 [data]
  (->> data
       lines
       (map extrapolate)
       (reduce +)))

(defn part2 [data]
  (->> data
       lines
       (map reverse)
       (map extrapolate)
       (reduce +)))

