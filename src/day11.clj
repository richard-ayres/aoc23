(ns day11
  (:require [clojure.string :as str]
            [clojure.math.combinatorics :as combo]))

(defn find-galaxies [universe]
  (filter (fn [[x y]] (not= \. (get-in universe [y x])))
          (for [x (range 0 (count (get universe 0)))
                y (range 0 (count universe))]
            [x y])))

(defn manhattan-distance [[x1 y1] [x2 y2]]
  (+ (abs (- x1 x2)) (abs (- y1 y2))))

(defn count-empty
  "Count the number of empty rows or columns before the given galaxy"
  [galaxies axis galaxy]
  (count (filter true?
                 (map #(empty? (filter (fn [tg] (= (tg axis) %)) galaxies))
                      (range 0 (galaxy axis))))))

(defn expand-galaxies
  "Expand empty rows and columns in the universe by n"
  [n galaxies]
  (let [count-empty (partial count-empty galaxies)]
    (map (fn [[x y]] [(+ x (* (dec n) (count-empty 0 [x y])))
                      (+ y (* (dec n) (count-empty 1 [x y])))])
         galaxies)))

(defn runit [N data]
  (->> data
       str/split-lines
       (mapv vec)
       find-galaxies
       (expand-galaxies N)
       (#(combo/combinations % 2))           ;; pairs
       (map #(apply manhattan-distance %))
       (reduce +)))

(def part1 (partial runit 2))
(def part2 (partial runit 1000000))

