(ns day05
  (:require [clojure.string :as str]))

(defn- get-seeds [lines]
  (->> (first lines)
       (re-seq #"\d+")
       (map parse-long)))

(defn- read-map
  "return seq of [dest source source-end] where source-end is 1 past the last value"
  [lines]
  (loop [lines (drop 1 lines)     ;; skip label
         the-map []]
    (if-not (seq (first lines))
      [the-map (drop 1 lines)]    ;; skip empty line (or nil)
      (recur (rest lines)
             (conj the-map
                   (vec
                    (let [[dest source length] (map parse-long (re-seq #"\d+" (first lines)))]
                      [dest source (+ source length)])))))))

(defn- mget [the-map value]
  (let [[m-dest m-source _] (first (filter (fn [[_ source s-end]] (and (>= value source)
                                                                       (< value s-end)))
                                           the-map))]
    (if (nil? m-dest)
      value
      (+ m-dest (- value m-source)))))

(defn- seeds->part2 [seed-ranges]
  (loop [seed-ranges seed-ranges
         seeds []]
    (if-not (seq seed-ranges)
      seeds
      (recur (drop 2 seed-ranges)
             (conj seeds
                   (let [[start length] (take 2 seed-ranges)]
                     [start (+ start length)]))))))

(defn- map-finder [[r-start r-end] [_ m-start m-end]]
  (or (and (>= r-start m-start)
           (< r-start m-end))
      (and (>= r-end m-start)
           (< r-end m-end))))

(defn- map-range [the-map the-range]
  (loop [result '()
         rs (list the-range)]
    (if-not (seq rs)
      result
      (let [[r-start r-end] (first rs)
            [m-dest m-source m-end] (first (filter (partial map-finder [r-start r-end]) the-map))]
        (cond
          ;; range is not in any map, just put it on results list
          (nil? m-dest)                                (recur (cons [r-start r-end] result) (rest rs))
          ;; range is entirey within map
          (and (>= r-start m-source) (<= r-end m-end)) (recur (cons [(+ m-dest (- r-start m-source)) (+ m-dest (- r-end m-source))] result)
                                                              (rest rs))
          ;; range overlaps front of map
          (<= r-end m-end)                             (recur (cons [m-dest (+ m-dest (- r-end m-source))] result)
                                                              (cons [r-start (dec m-source)] (rest rs)))
          ;; range overlaps end of map
          (>= r-start m-source)                        (recur (cons [(+ m-dest (- r-start m-source)) (+ m-end (- m-dest m-source))] result)
                                                              (cons [m-end r-end] (rest rs)))
          ;; shouldn't get here, put it back on results list
          :else (recur (cons [r-start r-end] result) (rest rs)))))))

(defn- get-maps [lines]
  (let [[seed->soil  maps] (read-map (drop 2 lines))
        [soil->fert  maps] (read-map maps)
        [fert->water maps] (read-map maps)
        [water->light maps] (read-map maps)
        [light->temp  maps] (read-map maps)
        [temp->humid maps] (read-map maps)
        [humid->loc      _] (read-map maps)]
    [seed->soil soil->fert fert->water
     water->light light->temp temp->humid
     humid->loc]))

(defn- seed->loc [maps seed]
  (let [[seed->soil
         soil->fert
         fert->water
         water->light
         light->temp
         temp->humid
         humid->loc] maps]
    (->> seed
         (mget seed->soil)
         (mget soil->fert)
         (mget fert->water)
         (mget water->light)
         (mget light->temp)
         (mget temp->humid)
         (mget humid->loc))))

(defn- rget [the-map ranges]
  (mapcat #(map-range the-map %) ranges))

(defn- range->loc [maps seed-ranges]
  (let [[seed->soil
         soil->fert
         fert->water
         water->light
         light->temp
         temp->humid
         humid->loc] maps]
    (->> seed-ranges
         (rget seed->soil)
         (rget soil->fert)
         (rget fert->water)
         (rget water->light)
         (rget light->temp)
         (rget temp->humid)
         (rget humid->loc))))

(defn part1 [data]
  (let [lines (str/split-lines data)
        seeds (get-seeds lines)
        maps (get-maps lines)]
    (->> seeds
         (map (partial seed->loc maps))
         (reduce min))))

(defn part2 [data]
  (let [lines (str/split-lines data)
        seeds (get-seeds lines)
        maps (get-maps lines)]
    (->> seeds
         seeds->part2
         (range->loc maps)
         (map first)
         (reduce min))))

