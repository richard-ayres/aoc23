(ns day07
  (:require [clojure.string :as str]
            [aoc.math :as math]
            [aoc.coll :as coll]))

(defn- nil-to-zero [x] (if (nil? x) 0 x))

(defn- line->hand
  "Convert a line of text into a [hand bid] vector"
  [line]
  (let [[hand bid] (str/split line #" ")]
    [hand (parse-long bid)]))

(def card->rank
  {\2 2 \3 3 \4 4 \5 5 \6 6
   \7 7 \8 8 \9 9 \T 10 \J 11
   \Q 12 \K 13 \A 14})

(def card->rank2
  "Card rank for part 2 (J = 1)"
  (assoc card->rank \J 1))

(def all-cards
  (keys card->rank))

(defn- compare-cards
  "Comparision function for two cards, given a card->rank function"
  [card->rank a b]
  (math/sgn (- (card->rank a) (card->rank b))))

(defn- tie-breaker
  "The two hands have the same rank, so compare their cards in order as per AoC"
  [card->rank as bs]
  (let [compare-cards (partial apply compare-cards card->rank)]
    (nil-to-zero
     (->> (map vector as bs)          ;; zip together the cards from each hand
          (map compare-cards)         ;; map each pair into their comparison value
          (remove zero?)              ;; filter out zeros
          first))))                   ;; return the first result

(defn- hand->rank
  "Rank the given hand, 1 to 7"
  [h]
  (let [h (frequencies h)          ;; convert to map of card to count
        n (count h)                ;; number of distinct cards in hand
        m (apply max (map val h))] ;; largest number of a single card
    (cond
      (= n 1)                      7 ;; five of a kind
      (and (= n 2) (= m 4))        6 ;; four of a kind
      (and (= n 2) (= m 3))        5 ;; full house
      (and (= n 3) (= m 3))        4 ;; three of a kind
      (and (= n 3) (= m 2))        3 ;; two pair
      (= n 4)                      2 ;; one pair
      :else                        1)))

(defn- hand->max_rank
  "Find the highest rank when replacing the joker"
  [hand]
  (->> all-cards
       (remove (partial = \J))      ;; not \J
       (map #(replace {\J %} hand)) ;; replace \J in hand with each card
       (map hand->rank)             ;; rank each new hand
       (reduce max)))               ;; return highest rank

(defn- compare-hands
  "Compare two hands using the provided ranker function and card to value map"
  [hand->rank card->rank [a _] [b _]]
  (let [result (- (hand->rank a) (hand->rank b))]
    (if (zero? result)
      (tie-breaker card->rank a b)
      (math/sgn result))))

(defn- total-winnings [hand->rank card->rank lines]
  "Calculate total winnings for the lines according to the provided
  hand and card ranker functions"
  (->> lines
       (map line->hand)
       (sort (partial compare-hands (memoize hand->rank) card->rank))
       (coll/enumerate 1)
       reverse
       (map (fn [[rank [_ bid]]] (* bid rank)))
       (reduce +)))

(defn part1 [data]
  (total-winnings hand->rank card->rank (str/split-lines data)))

(defn part2 [data]
  (total-winnings hand->max_rank card->rank2 (str/split-lines data)))

