(ns aoc.route-finder
  (:require [aoc.coll :as coll])
  (:require [clojure.spec.alpha :as s]))

(defrecord Node [loc        ;; this node's location
                 previous   ;; the previous node
                 score])    ;; this node's score

(defn unwind [node]
  "Return a sequence of nodes"
  (->> node
       (iterate :previous)
       (take-while (comp not nil?))
       (reverse)))

(defn make-route-result [final-node]
  (if (or (empty? final-node) (nil? final-node))
    nil
    {:route (->> (unwind final-node)
                 (drop 1)
                 (map :loc))
      :score (:score final-node)}))

(defn finder
  ([start finish get-neighbours] (finder start finish get-neighbours false nil))
  ([start finish get-neighbours can-revisit] (finder start finish get-neighbours can-revisit nil))
  ([start finish
    get-neighbours
    can-revisit
    heuristic]

   (defn load-neighbours [node seen seen-by-visit]
    (->> (get-neighbours node)
        ;; make score cumulative
         (map (fn [[loc score]] [loc (+ (:score node) score)]))
         ;; remove already seen locations
         (filter (fn [[loc _]] (or can-revisit (not (contains? seen loc)))))
         ;; remove if loc has been visited WITH THIS SCORE
         (remove (fn [[loc total-score]] (and can-revisit (contains? seen-by-visit [loc total-score]))))
         ;; create new Node
         (map (fn [[loc total-score]] (Node. loc node total-score)))))

   (def node-score
     (if (nil? heuristic)
       :score
       (fn [node] (+ (heuristic (:loc node)) (:score node)))))

   (def node-push (partial coll/priority-queue-push-by node-score))
   (def node-pop coll/priority-queue-pop)

   (defn finished? [node]
     (= (:loc node) finish))

   (defn find-route 
     ([start] (find-route [(Node. start nil 0)] #{start} #{[start 0]}))
     ([q seen seen-by-visit]
      (let [[node q'] (node-pop q)]
        (cond
          (nil? node) nil
          (finished? node) node
          :else (let [neighbours (load-neighbours node seen seen-by-visit)
                      q'' (if (empty? neighbours) q' (apply node-push q' neighbours))
                      seen' (conj seen (:loc node))
                      seen-by-visit' (conj seen-by-visit [(:loc node) (:score node)])]
                   (recur q'' seen' seen-by-visit'))))))

   (make-route-result (find-route start))))


(defn dijkstra [start finish get-neighbours]
  (finder start finish
          #(get-neighbours (:loc %))
          false
          nil))

(defn manhattan-distance [[x y] [x' y']]
  (+ (abs (- x x')) (abs (- y y'))))

(defn a-star
  ([start finish get-neighbours] (a-star manhattan-distance start finish get-neighbours))
  ([distance start finish get-neighbours] ;; [Point] -> sequence of [Point score])
   (finder start finish
           #(get-neighbours (:loc %))
           false
           (partial distance finish))))

(defn bfs [start finish get-neighbours]    ;; [loc] -> sequence of loc
  (finder start finish
          #(map (fn [n] [n 1]) (get-neighbours (:loc %)))
          false
          nil))

(defn dfs [start finish
           get-neighbours]

  (defn flatmap [f coll] (flatten (map f coll)))
  (defn min-by [f coll] (if (empty? coll)
                          '()
                          (first (sort-by f coll))))

  (defn search
   ([] (search #{} (list (Node. start nil 0))))
   ([seen stack]
    (let [current (first stack)]
      (if (nil? current)
        nil
        (if (= (:loc current) finish)
          current
          (->> (get-neighbours (:loc current))
               (filter (fn [[next-loc _]] (not (contains? seen next-loc))))
               (flatmap (fn [[next-loc next-score]] (search (conj seen (:loc current))
                                                            (cons (Node. next-loc current (+ next-score (:score current))) stack))))
               (min-by :score)))))))

  (make-route-result (search)))

;; Test route finder
(def t1 {:AA {:BB 2 :CC 4}
         :BB {:CC 6 :AA 1}
         :CC {:DD 2}
         :DD {:CC 2}
         :aa {:BB 800 :bb 1}
         :bb {:DD 100 :AA 100 :BB 10}})

(def t2 {:a {:b 1}
         :b {:c 2}
         :c {:a 1}})

(s/check-asserts true)
;; BFS 1
(s/assert #(= '(:CC :DD) %) (:route (bfs :AA :DD #(keys (get t1 %))))) 
;; DFS 1
(s/assert #(= '(:CC :DD) %) (:route (dfs :AA :DD #(zipmap (keys (get t1 %)) (repeat 1)))))
;; DFS 2
(s/assert #(= '(:BB :CC) %) (:route (dfs :aa :CC #(zipmap (keys (get t1 %)) (repeat 1)))))
;; DFS - no route
(s/assert nil? (:route (dfs :AA :ZZ t1)))
;; BFS - no route
(s/assert nil? (bfs :AA :ZZ #(keys (get t1 %))))
;; 1
(s/assert #(= '(:CC :DD) %) (:route (dijkstra :AA :DD t1)))
;; 1b
(s/assert #(= 6 %) (:score (dijkstra :AA :DD t1)))
;; 2
(s/assert nil? (dijkstra :CC :BB t1))
;; 3
(s/assert #(= '(:b :c) %) (:route (dijkstra :a :c t2)))
;; 3b
(s/assert #(= 3 %) (:score (dijkstra :a :c t2)))
;; 4 - takes more, cheaper, steps
(s/assert #(= '(:bb :BB :AA :CC) %) (:route (dijkstra :aa :CC t1)))
;; 4 - but dfs
(s/assert (partial = '(:bb :BB :AA :CC)) (:route (dfs :aa :CC t1)))

