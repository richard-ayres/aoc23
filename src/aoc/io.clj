(ns aoc.io
  (:require [clojure.string :as str]))

(defn file->lines [infile]
  (->> infile
       slurp
       str/split-lines))

(defn resource->lines [rsrc]
  (->> (clojure.java.io/resource rsrc)
       slurp
       str/split-lines))
