(ns aoc.string
  (:require [clojure.spec.alpha :as s]))

(defn substr
  ([s start] (substr s start nil))
  ([s start end]
   (let [l (count s)
         end (cond
               (nil? end) l
               (< end 0) (+ l end)
               :else end)
         start (if (< start 0)
                 (+ l start)
                 start)]
     (subs s start end))))

(s/check-asserts true)
(s/assert (partial = "123456") (substr "123456" 0))
(s/assert (partial = "12345") (substr "123456" 0 -1))
(s/assert (partial = "23456") (substr "123456" 1))
(s/assert (partial = "2345") (substr "123456" 1 -1))
(s/assert (partial = "2345") (substr "123456" -5 -1))
(s/assert (partial = "6") (substr "123456" -1))

