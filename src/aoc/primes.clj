(ns aoc.primes
  (:require [clojure.spec.alpha :as s]))

(defn sieve
  "Sieve of Eratosthenes. Return all prime numbers up to N"
  [N]
  (loop [primes []
         composite? #{}
         p 2]
    (cond
      ;; Are we finished?
      (> p N)
      primes

      ;; Is p already marked as composite?
      (composite? p)
      (recur primes composite? (inc p))

      ;; Add p to the list of primes, and mark all multiples of p
      ;; from p^2 to N as composite
      :else
      (recur (conj primes p)
             (reduce conj composite?
                     (range (* p p) (inc N) p))
             (inc p)))))

(defn prime?
  "Is n prime?"
  [n]
  (and (>= n 2)
       (not-any? #(zero? (mod n %))
                 (range 2 (inc (/ n 2))))))

(defn prime-filter
  "Prime filter. Return all prime numbers up to N"
  [N]
  (filter prime? (take-while #(<= % N) (iterate inc 2))))

(defn next-prime [n]
  (first (filter prime? (iterate inc (inc n)))))

