(ns aoc.coll)

(defn map-values [f m]
  "Return a map that has the same keys as m, and the
  function f run on all values in m"
  (into {} (for [[k v] m] [k (f v)])))

(defn enumerate
  "Return a sequence of pairs with the first item in the pair
  an increasing sequence and the second item comes from xs:
    - (enumerate xs)   -> ([0 (first xs)] [1 (second xs)] [2 (nth xs 2)]) ...
    - (enumerate 5 xs) -> ([5 (first xs)] [6 (second xs)] [7 (nth xs 2)]) ..."
  ([xs] (enumerate 0 xs))
  ([off xs] (map vector (iterate inc off) xs)))

(defn zip [xs & ys]
  (apply map vector (cons xs ys)))

(defn foldLeft [f init xs]
  (reduce f (cons init xs)))

(defn foldRight [f init [x & xs]]
  (if (nil? x)
    init
    (f x (foldRight f init xs))))

(defn priority-queue-push-by
  "Push to a priority queue. The sequence is sorted in ascending order by
  the result of calling keyfn on the items in the queue.
  The first item in the sequence is the next item off the queue."
  ([keyfn coll item & more]
   (let [pushfn (partial priority-queue-push-by keyfn)]
     (reduce pushfn (pushfn coll item) more)))
  ([keyfn coll item]
   (let [k (keyfn item)
         [before after] (split-with #(< (keyfn %) k) coll)]
     (concat before (cons item after)))))

(def priority-queue-push
  "Push to a priority queue. The queue is sorted in ascending order
  by the value of its items."
  (partial priority-queue-push-by identity))

(defn priority-queue-pop [q]
  "Return a 2-vec containing the next item to come off the queue,
  and the queue without that item: [item queue']"
  [(first q) (rest q)])
