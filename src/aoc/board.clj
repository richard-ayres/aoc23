(ns aoc.board
  (:require [clojure.string :as str]))

(defn from-strings
  "Take a sequence of strings and return a
  two dimensional array of chars"
  [strings]
  (vec (->> strings
            (map vec))))

(defn bget [board x y]
  (get-in board [y x]))

(defn bset [board x y v]
  (assoc-in board [y x] v))

(defn width [board]
  (count (first board)))

(defn height [board]
  (count board))

(defn rotate-right
  "Rotate board by 90 degrees to the right"
  [board]
  (vec (map vec
            (let [w (width board)
                  h (height board)]
              (for [y (range 0 h)]
                (for [x (range 0 w)]
                  (get-in board [(- w x 1) y])))))))

(defn board-hash [board]
  (hash board))

(defn print-board
  "Print a vector of vectors"
  [board]
  (println (str/join "\n" (map #(str/join "" %) board))))

(defn print-points
  "Print a board represented as a sequence of points"
  [points]
  (if (not (set? points))
    (print-points (set points))
    (let [xmin (reduce min (map first points))
          ymin (reduce min (map second points))
          xmax (reduce max (map first points))
          ymax (reduce max (map second points))
          board (vec
                 (for [y (range ymin (inc ymax))]
                   (vec
                    (for [x (range xmin (inc xmax))]
                      (if (contains? points [x y])
                        \# \.)))))]
      (print-board board))))


