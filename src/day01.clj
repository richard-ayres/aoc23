(ns day01
  (:require [clojure.string :as str]))

(defn- do-replace [text]
  (let [word-numbers
        '(["one"   "o1e"]
          ["two"   "t2o"]
          ["three" "t3e"]
          ["four"  "f4r"]
          ["five"  "f5e"]
          ["six"   "s6x"]
          ["seven" "s7n"]
          ["eight" "e8t"]
          ["nine"  "n9e"])]
    (reduce
     (fn [text [word replacement]]
       (str/replace text word replacement))
     text
     word-numbers)))

(defn- text->numbers [text]
  (->> text
       (iterate do-replace)          ;; run do-replace continually
       (partition 2 1)               ;; create a sequence of pairs of consecutive results
       (drop-while #(apply not= %))  ;; iterate until pairs are the same
       first first))                 ;; return the finished text

(defn- runit
  ([data] (runit identity data))
  ([processor data]
   (->> data
        str/split-lines
        (map processor)
        (map #(re-seq #"\d" %))
        (map #(map parse-long %))
        (map #(+ (* 10 (first %)) (last %)))
        (reduce +))))

(defn part1 [data] (runit data))
(defn part2 [data] (runit text->numbers data))


