(ns day04
  (:require [clojure.set :as s]
            [clojure.string :as str]))

(defn- count->score [n]
  (if (zero? n) 0
      (bit-shift-left 1 (dec n))))

(defn- line->card
  "input text becomes a 2-vec of lists of numbers. First is the winning numbers,
  second is the numbers on the card"
  [text] (->> text
              (re-seq #"\d+|\|")           ;; sequence of numbers and "|"
              (map parse-long)             ;; sequence of ints and nil ("|" -> nil)
              (split-with (comp not nil?)) ;; split the sequence just before "|"
              (map (partial drop 1))))     ;; remove first item in each sequence (card id and nil)

(defn- card->matches [[winners actual]]
  (s/intersection (set winners) (set actual)))

(defn part1 [data]
  (->> data
       str/split-lines
       (map line->card)
       (map card->matches)
       (map count)
       (map count->score)
       (reduce +)))

(defn- to-card-hash [[winners numbers]]
  {(first winners) [(rest winners) (drop 1 numbers)]})

(defn- line->card2
  "input becomes a hash-map of id -> 2-vec of lists of numbers"
  [text] (->> text
              (re-seq #"\d+|\|")
              (map parse-long)
              (split-with (comp not nil?))
              to-card-hash))

(defn- card->match_count [card]
  (count (card->matches card)))

(defn- play-card [cards card-id]
  (let [card (get cards card-id)
        match_count (card->match_count card)]
    (range (inc card-id) (+ 1 card-id match_count))))

(defn- update-hand [hand won-cards multiple]
  (->>  won-cards
        (map #(list % (+ multiple (hand %))))
        flatten
        (apply hash-map)
        (merge hand)))

(defn part2 [data]
  (let [card-map (->> data
                      str/split-lines
                      (map line->card2)
                      (apply merge))
        max-card-id (apply max (keys card-map))]
    (reduce +
            (loop [hand (zipmap (keys card-map) (repeat 1))
                   card-id 1]
              (if (> card-id max-card-id)
                (vals hand)
                (recur (update-hand hand (play-card card-map card-id) (hand card-id))
                       (inc card-id)))))))

