(ns day13
  (:require [aoc.coll :as coll]
            [clojure.string :as str]))

(defn transpose [m]
  (apply mapv vector m))

(defn read-block [text]
  (vec (->> text
            str/split-lines
            (map #(mapv {\# 1 \. 0} %)))))

(defn blocks
  "blocks is a sequence of matrices (vectors of vectors)"
  [data]
  (->> data
       (#(str/split % #"\n\n"))  ;; split into blocks
       (map read-block)))

(defn get-horizontal-reflection
  ([block] (get-horizontal-reflection 0 block))
  ([max-errors block]
   (defn error-check [ts]
     (= max-errors (/ (count (remove #(apply = %) ts)) 2)))

   (defn get-line-pairs [off]
     (let [n (min off (- (count block) off))]
       (coll/zip (map #(get block %) (reverse (range (- off n) off)))
                 (map #(get block %) (range off (+ off n))))))

   (first (filter (comp error-check get-line-pairs)
                  (range 1 (count block))))))

(defn get-vertical-reflection [max-errors block]
  (->> block
       transpose
       (get-horizontal-reflection max-errors)))

(defn get-reflection-value
  ([block] (get-reflection-value 0 block))
  ([max-errors block]
   (let [horiz (get-horizontal-reflection max-errors block)]
     (if (not (nil? horiz))
       (* 100 horiz)
       (get-vertical-reflection max-errors block)))))

(defn part1 [data]
  (->> data
       blocks
       (map get-reflection-value)
       (reduce +)))

(defn smudge [block]
  "Return a sequence of blocks with one item changed"
  (let [h (count block) w (count (get block 0))]
    (for [y (range 0 h) x (range 0 w)]
      (assoc block y
             (update (get block y) x #(- 1 %))))))

(defn get-reflection-value2 [block]
  (let [not_h (get-horizontal-reflection block)
        h (first (remove #(or (nil? %) (= % not_h))
                         (for [block (smudge block)]
                           (get-horizontal-reflection block))))]
    (if (not (nil? h))
      (* 100 h)
      (let [not_v (get-vertical-reflection 0 block)
            v (first (remove #(or (nil? %) (= % not_v))
                             (for [block (smudge block)]
                               (get-vertical-reflection 0 block))))]
        (if (nil? v) (if (nil? not_v) not_h not_v) v)))))

;; (println "Part 2:"
;;          (->> blocks
;;               (map get-reflection-value2)
;;               (reduce +)))
