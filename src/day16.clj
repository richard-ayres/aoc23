(ns day16
  (:require [clojure.string :as str]))

(defn- load-contraption [data]
  (->> data
       str/split-lines
       (mapv vec)))
       ;; vec))

(defn- stationary? [[_ _ dx dy]]
  (and (zero? dx) (zero? dy)))

(defn- dimensions [vvs]
  [(count (vvs 0)) (count vvs)])

(def next-beams
  (memoize
   (fn [contraption beam]
     (let [[x y dx dy] beam
           x' (+ x dx)
           y' (+ y dy)
           beam' [x y 0 0]]
       (case (get-in contraption [y' x'])
         nil [beam']
         \. [beam' [x' y' dx dy]]
         \| (if (zero? dy)
                ;; moving left->right or right->left
              [beam' [x' y' 0 1] [x' y' 0 -1]]
                ;; moving up->down or down->up
              [beam' [x' y' 0 dy]])
         \- (if (zero? dx)
                ;; moving up->down or down->up
              [beam' [x' y' 1 0] [x' y' -1 0]]
                ;; moving left->right or right->left
              [beam' [x' y' dx 0]])
         \\ [beam' [x' y' dy dx]]
         \/ [beam' [x' y' (- 0 dy) (- 0 dx)]])))))

(defn- process-beams [in-contraption? next-beams beams]
  (set
   (filter in-contraption?
           (loop [beams' (set beams)
                  beams (remove stationary? beams)]
             (if-some [[beam & remaining] beams]
               (recur (reduce conj beams' (next-beams beam))
                      remaining)
               beams')))))

(defn- all-beams
  ([contraption] (all-beams contraption [-1 0 1 0]))
  ([contraption start-beam]

   (let [[width height] (dimensions contraption)
         next-beams (partial next-beams contraption)]
     (letfn [(in-contraption? [[x y _ _]]
               (and (>= x 0) (>= y 0) (< x width) (< y height)))]
       (loop [beams #{start-beam}]
         (let [beams' (process-beams in-contraption? next-beams beams)]
           (if (= beams' beams)
             beams
             (recur beams'))))))))

(defn- make-map [width height energized]
  (vec
   (for [y (range 0 height)]
     (vec
      (for [x (range 0 width)]
        (if (contains? energized [x y])
          \#
          \.))))))

(defn draw-energized [contraption beams]
  (let [[width height] (dimensions contraption)]
    (map #(str/join "" %)
         (let [energized (set (map (fn [[x y _ _]] [x y]) beams))
               energized-map (make-map width height energized)]
           energized-map))))

(defn- count-energized [contraption start-beam]
  (->> (all-beams contraption start-beam)
       (map (fn [[x y _ _]] [x y]))
       distinct
       count))

(defn part1 [data]
  (count-energized (load-contraption data) [-1 0 1 0]))

(defn- all-start-beams [width height]
  (concat (for [x (range 0 width)]  [x -1 0 1])
          (for [x (range 0 width)]  [x height 0 -1])
          (for [y (range 0 height)] [-1 y 1 0])
          (for [y (range 0 height)] [width y -1 0])))

(defn part2 [data]
  (let [contraption (load-contraption data)]
    (->> (apply all-start-beams (dimensions contraption))
         (map (partial count-energized contraption))
         (reduce max))))

;; (time
;;  (println (part1 (slurp (clojure.java.io/resource "day16.txt")))))
