(ns day12
  (:require [clojure.string :as str]))

(defn line->row
  ([text] (line->row 1 text))
  ([n text]
   (let [[springs chk] (str/split text #" ")
         chk (str/join "," (repeat n chk))
         chk (vec (map parse-long (str/split chk #",")))
         springs (str/join "?" (repeat n springs))
         springs (vec springs)]
     [springs chk])))

(defn contains [x xs] (some #(= % x) xs))

(defn bool->int [f] (if f 1 0))

;; The following translated from the python at
;; https://www.reddit.com/r/adventofcode/comments/18ge41g/comment/kd0oj1t/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
(def count-valid
  (memoize
   (fn [s c]

; @cache # too slow without the cache - stpres function values
; def numlegal(s,c):

;     s = s.lstrip('.') # ignore leading dots
     (let [s (drop-while #(= % \.) s)]

       (cond
         ;     # ['', ()] is legal
         ;     if s == '':
         ;         return int(c == ()) 
         (empty? s) (bool->int (empty? c))

;     # [s, ()] is legal so long as s has no '#' (we can convert '?' to '.')
;     if c == ():
;         return int(s.find('#') == -1) 
         (empty? c) (bool->int (not (contains \# s)))

;     # s starts with '#' so remove the first spring
;     if s[0] == '#':
         (= \# (first s))
         (cond
          ;         if len(s) < c[0] or '.' in s[:c[0]]:
          ;             return 0 # impossible - not enough space for the spring
           (or (< (count s) (first c))
               (contains \. (take (first c) s))) 0
          ;         elif len(s) == c[0]:
          ;             return int(len(c) == 1) #single spring, right size
           (= (count s) (first c))               (bool->int (= 1 (count c)))
          ;         elif s[c[0]] == '#':
          ;             return 0 # springs must be separated by '.' (or '?') 
           (= (nth s (first c)) \#)              0
          ;         else:
          ;             return numlegal(s[c[0]+1:],c[1:]) # one less spring
           :else                                 (recur (drop (inc (first c)) s) (rest c)))
       ;     # numlegal springs if we convert the first '?' to '#' + '.'
       ;     return numlegal('#'+s[1:],c) + numlegal(s[1:],c)
         :else (+ (count-valid (cons \# (rest s)) c)
                  (count-valid (rest s) c)))))))

(defn runit [line->row data]
  (->> data
       str/split-lines
       (map line->row)
       (map #(apply count-valid %))
       (reduce +)))

(defn part1 [data] (runit line->row data))
(defn part2 [data] (runit (partial line->row 5) data))
