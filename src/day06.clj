(ns day06
  (:require [clojure.string :as str]
            [clojure.math :as m]))

(defn- count-winning-delays [T D]
  (let [√b2-4ac (m/sqrt (- (* T T) (* 4 D)))
        lo (int (m/floor (/ (- T √b2-4ac) 2)))
        hi (int (m/ceil (/ (+ T √b2-4ac) 2)))]
    (- hi lo 1)))

(defn- get-line1 [line]
  (map parse-long (re-seq #"\d+" line)))

(defn- get-line2 [line]
  (parse-long (str/join "" (re-seq #"\d+" line))))

(defn part1 [data]
  (let [lines (str/split-lines data)
        times (get-line1 (first lines))
        distances (get-line1 (second lines))]
    (->> [times distances]
         (apply map vector)
         (map #(apply count-winning-delays %))
         (reduce *))))

(defn part2 [data]
  (let [lines (str/split-lines data)]
    (count-winning-delays
     (get-line2 (first lines))
     (get-line2 (second lines)))))
