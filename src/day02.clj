(ns day02
  (:require [clojure.string :as str]))

(defn- text->game [text]
  (let [matches (->> text
                     (#(str/split % #"\s*,\s*"))
                     (map #(re-matches #"(\d+) (\w+)" %)))]
    (loop [cubes {} matches matches]
      (if-some [[match & remaining] matches]
        (recur (merge cubes {(last match) (parse-long (second match))}) remaining)
        cubes))))

(defn- line->games
  "Parse a line of input into a 2-item vector of [id (list of games)]"
  [line]
  (let [[_ id games] (re-matches #"Game (\d+): (.+)$" line)]
    [(parse-long id) (map text->game (str/split games #"\s*;\s*"))]))

(defn- valid-game? [game]
  (let [m {"red" 12 "green" 13 "blue" 14}]
    (every? (fn [[colour max]] (<= (get game colour 0) max)) m)))

(defn- games->power [game]
  (reduce *
          (for [colour (list "red" "green" "blue")]
            (apply max (map #(get % colour 0) game)))))

(defn- games
  "List of [id (list of games)]"
  [data]
  (->> data
       str/split-lines
       (map line->games)))

(defn part1 [data]
  (->> data
       games
       (filter #(every? valid-game? (second %)))
       (map first)
       (reduce +)))

(defn part2 [data]
  (->> data
       games
       (map second)
       (map #(games->power %))
       (reduce +)))
