(ns aoc
  (:require [clojure.java.io :as io]))
            ;; day01 day02 day03 day04
            ;; day05 day06 day07 day08
            ;; day09 ;;day10 
            ;; day11 day12
            ;; day13 day14 day15 day16
            ;; day17 day18 day19))

(def parts
  (list
   [1 day01/part1 day01/part2 identity]))
   ;; [2 day02/part1 day02/part2 identity]
   ;; [3 day03/part1 nil identity]
   ;; [4 day04/part1 day04/part2 identity]
   ;; [5 day05/part1 day05/part2 identity]))
   ;; [6 day06/part1 day06/part2 identity]
   ;; [7 day07/part1 day07/part2 identity]
   ;; [8 day08/part1 day08/part2 identity]
   ;; [9 day09/part1 day09/part2 identity]
   ;; [10 nil nil identity]
   ;; [11 day11/part1 day11/part2 identity]
   ;; [12 day12/part1 day12/part2 identity]
   ;; [13 day13/part1 nil identity]
   ;; [14 day14/part1 day14/part2 identity]
   ;; [15 day15/part1 day15/part2 identity]
   ;; day 16 is too long
   ;; [16 day16/part1 nil identity]
   ;; [17 nil nil identity]
   ;; [18 day18/part1 day18/part2 day18/preprocess]
   ;; [19 day19/part1 day19/part2 day19/preprocess]))

(defn- get-data
  ([day] (get-data false day))
  ([sample? day]
   (slurp (if sample?
            (format "sample%02d.txt" day)
            (clojure.java.io/resource (format "day%02d.txt" day))))))

(defn- file-exists? [filename]
  (.exists (clojure.java.io/file filename)))

(defn- run-day
  [[day part1 part2 pre-process]]
  (let [data (pre-process (get-data day))
        f1 (future (if (nil? part1) nil (part1 data)))
        f2 (future (if (nil? part2) nil (part2 data)))]
    [day @f1 @f2]))

;; (range 1 26)

(doseq [[day file] (->> (range 1 2) ;;(range 1 26)
                        (map #(vector % (format "src/day%02d.clj" %)))
                        (filter #(file-exists? (second %))))]
  (load-file file)
  (let [namespace (symbol (format "day%02d" day))
        publics (ns-publics namespace)
        preprocess (get publics "pre-process" identity)
        part1 (get publics 'part1 (constantly nil))
        part2 (get publics 'part2 (constantly nil))
        data (preprocess (get-data day))]
    [(part1 data) (part2 data)]))
    ;; (println "Part 1:" (part1 data))
    ;; (println "Part 2:" (part2 data))))

(time
 (doseq [result (pmap run-day (filter #(not= 16 (first %)) parts))]
   (let [[day result1 result2] result]
     (println (format "Day %2d /1 %s" day result1))
     (println (format "       /2 %s" result2)))))

(file-exists? "src/day01.clj")

(load-file "src/day01.clj")
(map type (vals (ns-publics 'day01)))
(ns-publics (symbol "day01"))

;; (try
;;   (load-file "src/day20.clj")
;;   (catch java.lang.FileNotFoundException e (str "File not  found")))
